import { DeleteResult, getCustomRepository } from "typeorm";
import AppError from "../../errors/appError";
import UserRepository from "../../repositories/UserRepository";


interface Request {
    id: string;
};

class DeleteUserService {
    public async execute({id}: Request): Promise<DeleteResult> {
        const userRepository = getCustomRepository(UserRepository);

        const user = await userRepository.findOne({
            where: {
                id,
            }
        });

        if(!user) {
            throw new AppError("Not found any user with this ID.", 404);
        };

        return userRepository.delete(id);
    };
};

export default DeleteUserService;