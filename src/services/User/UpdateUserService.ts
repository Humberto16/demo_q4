import { compare, hash } from "bcryptjs";
import { getCustomRepository } from "typeorm";
import AppError from "../../errors/appError";
import User from "../../models/User";
import UserRepository from "../../repositories/UserRepository";

interface Request {
    user_id: string;
    name: string;
    email: string;
    password: string;
    old_password: string;
};


export default class UpdateUserService {
    public async execute({user_id, name, email, password, old_password}: Request): Promise<User>{
        const userRepository = getCustomRepository(UserRepository);

        const user = await userRepository.findOne({
            where: {
                id: user_id,
            }
        });

        if(!user){
            throw new AppError("User not found.", 404);
        };

        const userWithUpdatedEmail = await userRepository.findByEmail(email);

        if(userWithUpdatedEmail && userWithUpdatedEmail.id !== user_id) {
            throw new AppError("Email already in use.");
        };

        name ? (user.name = name) : user.name;
        email ? (user.email = email) : user.email;

        if(password && !old_password) {
            throw new AppError(
                "You need to inform the old password to set a new password.");
        };

        if(password && old_password) {
            const checkPassword = await compare(old_password, user.password);

            if(!checkPassword) {
                throw new AppError("Old password does not match.");
            };

            user.password = await hash(password, 8);
        };

        await userRepository.save(user);

        return user;

    };
};
