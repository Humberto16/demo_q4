import { getRepository } from "typeorm";
import AppError from "../../errors/appError";
import Order from "../../models/Order";
import OrderProducts from "../../models/OrderProduct";
import Product from "../../models/Products";


interface Request {
    desk: string;
    products_id: string[];
};

export default class CreateOrdersService {
    public async execute({desk, products_id}: Request): Promise<Order> {

        const orderRepository = getRepository(Order);
        const orderProductsRepository = getRepository(OrderProducts);
        const productsRepository = getRepository(Product);

        products_id.forEach( async (product_id) => {
            const products = await productsRepository.findOne({
                where: {
                    id: product_id
                }
            }).catch((err) => {
                new AppError("Inválid list of products Id's. ")
            });
        });

        const order = orderRepository.create({
            desk,
        });

        await orderRepository.save(order);

        products_id.forEach( async (product_id) => {
            const orderProduct = orderProductsRepository.create({
                order: {
                    id: order.id
                },
                product: {
                    id: product_id
                },
            });
            await orderProductsRepository.save(orderProduct);
        });
        return order;
    };
};