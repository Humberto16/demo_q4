import { DeleteResult, getRepository } from "typeorm";
import AppError from "../../errors/appError";
import Product from "../../models/Products";



interface Request {
    id: string;
    name: string;
    price: number;
    description: string;
};

class UpdateProductService {
    public async execute({id, name, description, price}: Request): Promise<Product> {
        const productRepository = getRepository(Product);

        const product = await productRepository.findOne({
            where: {
                id,
            }
        });

        if(!product) {
            throw new AppError("Not found any product with this ID.", 404);
        };

        price ? (product.price = price) : product.price;
        name ? (product.name = name) : product.name;
        description ? (product.description = description) : product.description;

        return productRepository.save(product);
    };
};

export default UpdateProductService;