import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import AppError from "../errors/appError";
import authConfig from "../config/auth";

interface TokenPayload {
    iat: number;
    exp: number;
    sub: string;
}

export const ensureAuth = (req: Request, resp: Response, next: NextFunction) => {
    const authHeader = req.headers.authorization;

    if(!authHeader) {
        throw new AppError("JWT is missing.", 401);
    };

    try{
        const [, token] = authHeader.split(" ");
        const { secret } = authConfig.jwt;

        const decode = verify(token, secret);
        const { sub } = decode as TokenPayload;

        req.user = {
            id: sub,
        };
        return next();

    }catch(err) {
        throw new AppError("JWT expired or sended in a wrong way");
    };

};