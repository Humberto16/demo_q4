import { Router } from "express";
import { getRepository } from "typeorm";
import Order from "../models/Order";
import CreateOrdersService from "../services/Orders/CreateOrdersService";
import * as yup from "yup";
import AppError from "../errors/appError";


const orderRouter = Router();

orderRouter.get("/", async (req, res) => {
    const orderRepository = getRepository(Order);

    const orders = await orderRepository.find();

    res.json(orders);

});

orderRouter.post("/", async (req, res) => {
    const { desk, products_id } = req.body;

    const schema = yup.object().shape({
        desk: yup.string().required(),
        products_id: yup.array().required()
    });

    await schema.validate(req.body).catch(({errors}) => {
        throw new AppError(errors);
    })

    const orderCreate = new CreateOrdersService();

    const order = await orderCreate.execute({
        desk,
        products_id
    });

    res.status(201).json(order);
});

export default orderRouter;