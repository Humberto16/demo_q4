import { Router } from "express";
import orderRouter from "./orders.routes";
import productRouter from "./products.routes";
import sessionRouter from "./session.routes";
import userRouter from "./user.routes";

const routes = Router();

routes.use("/users", userRouter);
routes.use("/session", sessionRouter);
routes.use("/products", productRouter);
routes.use("/orders", orderRouter);

export default routes;