import { Router } from "express";
import { getRepository } from "typeorm";
import { ensureAuth } from "../middlewares/ensureAuth";
import Product from "../models/Products";


import CreateProductService from "../services/Products/CreateProductService";
import DeleteProductService from "../services/Products/DeleteProductsService";
import UpdateProductService from "../services/Products/UpdateProductService";

const productRouter = Router();

productRouter.get("/", async (req, res) => {
    const productRepository = getRepository(Product);

    const products = await productRepository.find();

    return res.json(products);
});

productRouter.use(ensureAuth);

productRouter.post("/", async (req, res) => {
    const {name, price, description} = req.body;
    const createProduct = new CreateProductService();

    const product = await createProduct.execute({
        name,
        price,
        description,
    });
    return res.status(201).json(product);

});

productRouter.patch("/:product_id", async (req, res) => {
    const {product_id} = req.params;
    const {name, price, description} = req.body;

    const updateProduct = new UpdateProductService();

    const UpdatedProduct = await updateProduct.execute({
        id: product_id,
        name,
        price,
        description
    });
});

productRouter.delete("/", (req, res) => {

});

export default productRouter;