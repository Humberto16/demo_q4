import { classToClass } from "class-transformer";
import { Router } from "express";
import { getCustomRepository } from "typeorm";
import { ensureAuth } from "../middlewares/ensureAuth";
import UserRepository from "../repositories/UserRepository";
import CreateUserService from "../services/User/CreateUserService";
import DeleteUserService from "../services/User/DeleteUserService";
import UpdateUserService from "../services/User/UpdateUserService";


const userRouter = Router();

userRouter.post("/", async (req, res) => {
    const { name, email, password} = req.body;

    const createUser = new CreateUserService();

    const user = await createUser.execute({
        name,
        email,
        password,
    });

    return res.status(201).json(classToClass(user));
});

userRouter.use(ensureAuth);

userRouter.get("/", async (req, res) => {
    const userRepository = getCustomRepository(UserRepository);

    const users = await userRepository.find();

    return res.json(classToClass(users));
});

userRouter.patch("/profile", (req, res) => {
    const { name, email, password, old_password} = req.body;

    const updateUser = new UpdateUserService();

    const user = updateUser.execute({
        user_id: req.user.id,
        name,
        email,
        password,
        old_password,
    });

    return res.json(classToClass(user));

});

userRouter.delete("/profile", async (req, res) => {
    const deleteUser = new DeleteUserService();

    await deleteUser.execute({
        id: req.user.id,
    });

    return res.status(204).json();
});

export default userRouter;
