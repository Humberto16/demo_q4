import { 
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn, 
    OneToMany
} from "typeorm";
import OrderProducts from "./OrderProduct";

@Entity("orders")
class Order {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    desk: string;

    @OneToMany(() => OrderProducts, (orderProduct) => orderProduct.order, {
        eager: true
    })
    products: OrderProducts[];

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
};

export default Order;