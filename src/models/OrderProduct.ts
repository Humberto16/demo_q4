import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import Order from "./Order";
import Product from "./Products";


@Entity("order_products")
class OrderProducts {
    @PrimaryGeneratedColumn()
    id: string;

    @ManyToOne(() => Product)
    product: Product;

    @ManyToOne(() => Order)
    order: Order;

    @Column()
    productId: string;

    @Column()
    orderId: string;

    @CreateDateColumn()
    created_at: Date;

    @CreateDateColumn()
    updated_at: Date;
};

export default OrderProducts;