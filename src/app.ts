import express, { Request, Response, NextFunction} from "express";
import "express-async-errors"
import connection from "./database";
import router from "./routes/index";
import AppError from "./errors/appError";

connection();

const app = express();

app.use(express.json());
app.use(router);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    if(err instanceof AppError){
        return res.status(err.statusCode).json({
            status: "Error",
            message: err.message,
        });
    };

    return res.status(500).json({
        status: "Error",
        message: "Internal server error",
    });
});

app.listen(3000, () => {
    console.log("Runing...");
});

export default app;
